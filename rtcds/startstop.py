# start, stop and restart models
import sys
import time
import subprocess

import click

from .env import environment as env
from .util import list_host_models, check_model_in_host, epics_only_model, systemd_unit_status
from .log import log
from .kernel_module import is_module_loaded


def start_model(model: str, force: bool = False):
    """
    Start a model
    :param model: the name of the model to start
    :return: True if successful
    """
    log(f"### starting {model}")
    if not (force or check_model_in_host(model)):
        log(f"*** error: {model} can't be run on this host")
    else:
        cmd = f"sudo systemctl start rts@{model}.target".split()
        result = subprocess.run(cmd)
        if result.returncode == 0:
            log(f"waiting for {model} to finish starting")
            time.sleep(env.START_DELAY)
            if not epics_only_model(model):
                is_running = is_module_loaded(model)
            else:
                is_running = (systemd_unit_status(f"rts-epics@{model}") == "ON")
            if is_running:
                log(f"### {model} started")
                return True
            else:
                log(f"Error: {model} failed to start")
                return False

        else:
            return False
    return False


def start_all_models():
    """
    Start all models meant to run on the current host
    :return: True if all models started
    """
    all_good = True
    models = list_host_models()
    for model in models:
        success = start_model(model)
        all_good = all_good and success
    return all_good


def stop_model(model: str):
    """
    Stop a model
    :param model: the name of the model to stop
    :return: True if successful
    """
    log(f"### stopping {model}")
    cmd = f"sudo systemctl stop rts@{model}.target".split()
    result = subprocess.run(cmd)
    if result.returncode == 0:
        attempts = 0
        while is_module_loaded(model):
            time.sleep(1)
            attempts += 1
            if attempts >= 120:
                log(f"Timed out waiting for {model} to stop")
                return False
        log(f"### {model} stopped")
        return True
    else:
        return False

def stop_all_models():
    """
    Stop all models running on the system
    :return: True if all models stopped
    """
    all_good = True
    models = list_host_models()

    # reverse so that IOP is removed last
    models.reverse()
    for model in models:
        success = stop_model(model)
        all_good = all_good and success
    return all_good


@click.command()
@click.option("-f", "--force", default=False, is_flag=True, help="force start on model not assigned to this host")
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="start all models")
def start(model, all_, force):
    """
    start models

    MODEL: the name of the models to start
    """
    success = False
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            success = start_model(m, force=force)
    if all_:
        if force:
            click.echo("WARNING: force option ignored when --all option is given")
        success = start_all_models()

    if not success:
        sys.exit(1)


@click.command()
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="stop all models")
def stop(model, all_: bool):
    """
    stop models

    MODEL: the name of the models to stop
    """
    success = False
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            success = stop_model(m)
    if all_:
        success = stop_all_models()

    if not success:
        sys.exit(1)


@click.command()
@click.argument("model", nargs=-1)
@click.option("--all", "all_", default=False, is_flag=True, help="restart all models")
def restart(model, all_: bool):
    """
    restart models

    MODEL: the name of the models to restart
    """
    success = False
    for m in model:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            success = stop_model(m)
            success = success and start_model(m)
    if all_:
        success = stop_all_models()
        success = success and start_all_models()

    if not success:
        sys.exit(1)
