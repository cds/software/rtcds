"""blog (build log) command"""
import sys
import subprocess

import click

from .env import environment as env


@click.command()
@click.argument("model")
@click.option("-i", "--info", is_flag=True,
              help="print paths to logs")
def blog(model, info):
    """
    show last build log for a model

    MODEL: the name of the model
    """
    log_file = f"{env.RCG_BUILDD}/{model}.log"
    err_file = f"{env.RCG_BUILDD}/{model}_error.log"
    if info:
        subprocess.run(f"ls -al {log_file} {err_file}", shell=True,
                       stdout=sys.stdout, stderr=sys.stderr)
    else:
        try:
            with open(log_file, "rt") as f:
                sys.stdout.write(f.read())
            with open(err_file, "rt") as f:
                sys.stderr.write(f.read())
        except IOError as e:
            first_e = e
            log_file = f"{env.RCG_BUILDD}/models/{model}/logs/{model}.log"
            err_file = f"{env.RCG_BUILDD}/models/{model}/logs/{model}_error.log"
            try:
                with open(log_file, "rt") as f:
                    sys.stdout.write(f.read())
                with open(err_file, "rt") as f:
                    sys.stderr.write(f.read())
            except IOError as e:
                sys.stderr.write(str(first_e) + "\n")
                sys.stderr.write(str(e) + "\n")

