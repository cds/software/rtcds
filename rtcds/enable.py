"""commands for enabling or disabling models in systemd"""
import sys
import subprocess

import click


@click.command()
@click.argument("model", nargs=-1)
def enable(model: str):
    """Enable a model to start on boot

    MODEL: the model to enable.
    """
    failed = False
    for m in model:
        result = subprocess.run(f"sudo systemctl enable rts@{m}.target", shell=True,
                       stdout=sys.stdout, stderr=sys.stderr)
        if results.returncode != 0:
            failed = True
    if failed:
        sys.exit(1)


@click.command()
@click.argument("model", nargs=-1)
def disable(model: str):
    """Disable a model to start on boot

    MODEL: the model to enable.
    """
    failed = False
    for m in model:
        result = subprocess.run(f"sudo systemctl disable rts@{m}.target", shell=True,
                       stdout=sys.stdout)
        if result.returncode != 0:
            failed = True
    if failed:
        sys.exit(1)