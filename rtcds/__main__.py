"""
Command line tool for manipulating LIGO CDS real-time system
"""

import click

import sys
sys.argv[0] = 'rtcds'

from . import __version__
from .env import environment, env
from .build import build
from .install import install
from .revert import revert
from .startstop import start, stop, restart
from .list import list_
from .enable import enable, disable
from .log_command import log
from .blog import blog
from .lsmod import lsmod
from .status import status
from .showcards import showcards

# import signal


CONTEXT_SETTINGS = {
    'help_option_names': ['-h', '--help'],
}

ver = __version__
version = f"{ver}; RTS_VERSION={environment.RTS_VERSION}"

@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(version=version)
def rtcds():
    pass


rtcds.add_command(env)
rtcds.add_command(build)
rtcds.add_command(install)
rtcds.add_command(revert)
rtcds.add_command(start)
rtcds.add_command(stop)
rtcds.add_command(restart)
rtcds.add_command(list_)
if environment.ALLOW_MODEL_ENABLE:
    rtcds.add_command(enable)
rtcds.add_command(disable)
rtcds.add_command(log)
rtcds.add_command(blog)
rtcds.add_command(lsmod)
rtcds.add_command(status)
rtcds.add_command(showcards)

# def sigint_handler(signal, frame):
#     print("SIGINT")
#     build.cleanup()
#     sys.exit(1)
#
# signal.signal(signal.SIGINT, sigint_handler)

if __name__ == '__main__':
    rtcds()
