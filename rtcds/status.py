"""Handle the status command"""
import sys
import subprocess

import click

from .util import systemd_unit_status, epics_only_model, list_host_models
from .lsmod import list_modules


def show_model_status(models):
    """Show systemd status for all units associated with the given models.

    :param models: A list of model names
    """
    unit_templates = "rts@%s.target rts-awgtpman@%s.service rts-epics@%s.service rts-module@%s.service".split()
    epics_only_templates = "rts-epics@%s.service".split()
    units = []
    for model in models:
        if epics_only_model(model):
            units += [u % model for u in epics_only_templates]
        else:
            units += [u % model for u in unit_templates]
    subprocess.run(["systemctl", "status"] + units, stdout=sys.stdout)


def show_system_status():
    """
    Determine, then print status for realtime models (systems) that should be running
    on this host.
    :return: True if every component of every model is running.
    """
    allok = True
    subsystems = "epics module awgtpman".split()
    format_str = "%-15s" * (len(subsystems) + 1)
    click.echo(format_str % tuple(["system"] + subsystems))
    bars = 15 * (len(subsystems) + 1)
    click.echo("-" * bars)

    for model in list_host_models():
        stats = []
        for sub_sys in subsystems:
            if epics_only_model(model) and sub_sys != "epics":
                stat = "N/A"
            else:
                stat = systemd_unit_status(f"rts-{sub_sys}@{model}")
                if stat != "ON":
                    allok = False
            stats.append(stat)
        click.echo(format_str % tuple([model] + stats))
    return allok


def show_streamer_status():
    """
    Determine, then print the status for the chain of process that stream
    real time data to the Data Acquisition System (DAQ).
    :return: True if all streaming process are running.
    """
    units = "rts-local_dc rts-transport@cps_xmit".split()
    allok = True
    for unit in units:
        stat = systemd_unit_status(unit)
        if stat != "ON":
            allok = False
        click.echo("%-18s %s" % (unit, stat))
    return allok


def show_status_overview():
    """Show high level information about running services
    """
    click.echo("Kernel Module Status")
    module_status = list_modules()
    module_status_text = module_status and "ALL LOADED" or "SOME MISSING"

    click.echo(f"Kernel Module Status = {module_status_text}")
    click.echo("")
    click.echo("")

    click.echo("System Status")
    sys_status = show_system_status()

    sys_status_text = sys_status and "ALL ACTIVE" or "DEGRADED"

    click.echo(f"Systems Status =       {sys_status_text}")
    click.echo("")
    click.echo("")

    click.echo("Streamer Status")
    streamer_status = show_streamer_status()
    streamer_status_text = streamer_status and "ALL ACTIVE" or "DEGRADED"

    click.echo(f"Streamer Status =      {streamer_status_text}")
    click.echo("")

    overall_status = module_status and sys_status and streamer_status
    overall_status_text = overall_status and "OK" or "DEGRADED"

    click.echo(f"Overall Status =       {overall_status_text}")


@click.command()
@click.argument("models", nargs=-1)
def status(models):
    """
    show status for system services

    MODELS: if a model or model name is given systemd status output is shown for the services associated
    with that model.
    If no model names are given, a high level overview of status for all models and other services is given.
    """
    if len(models) > 0:
        show_model_status(models)
    else:
        show_status_overview()
