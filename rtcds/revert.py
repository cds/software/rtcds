# revert models for rtcds
import os
import errno
import subprocess
import shutil
import sys
from datetime import datetime

import click

from .env import environment as env
from .util import list_models
from .log import log, error


def find_installations(model: str):
    """
    Find valid archives from older installs of a model
    :param mode" The name of the model
    """
    
    # Verify all expected archive directories are present
    target_dir = env.RCG_TARGET
    target_archive = os.path.join(target_dir, "target_archive", model)
    medm_archive = os.path.join(target_dir, "medm", "archive")
    gds_param_archive = os.path.join(target_dir, "target", "gds", "param", "archive")
    chans_daq_archive = os.path.join(target_dir, "chans", "daq", "archive")
    chans_filter_archive = os.path.join(target_dir, "chans", "filter_archive", model)
    for path in [target_dir, target_archive, medm_archive, 
                 gds_param_archive, chans_daq_archive, chans_filter_archive]:
        if not os.path.exists(path):
            print(f"Error : revert : Path {path} expected, but not found in target directory. Maybe there where no previous installations?")
            return {}

    
    valid_installs = {}

    # Find all valid target archive installations 
    for directory in os.listdir(target_archive):
        try:
            date_str = directory.split(model+"_", 1)[1]
            date_obj = datetime.strptime(date_str, '%y%m%d_%H%M%S')
        except:
            pass #Assume bad
        else:
            valid_installs[date_obj] = { "target_archive": False, "medm_archive": False, "gds_tpchn_archive": False, "daq_archive" : False, "filter_archive": False }
            valid_installs[date_obj]["target_archive"] = True;


    # Find all valid medm archives
    for directory in os.listdir(medm_archive):
        if directory.startswith(model + "_"):

            try:
                date_str = directory.split(model+"_", 1)[1]
                date_obj = datetime.strptime(date_str, '%y%m%d_%H%M%S')
                if date_obj in valid_installs:
                    valid_installs[date_obj]["medm_archive"] = True;
            except:
                pass #Skip bad names

    # Find all valid gds param archives 
    for (dirpath, dirnames, filenames) in os.walk(gds_param_archive):
        for file in filenames:

            #We don't revert testpoint.par File, in case other model was installed after this one

            #tpchn file
            tpchan_str = "tpchn_"+model+"_"
            if file.startswith(tpchan_str):
                try:
                    date_str = file.split(tpchan_str, 1)[1].strip(".par")
                    date_obj = datetime.strptime(date_str, '%y%m%d_%H%M%S')
                    if date_obj in valid_installs:
                        valid_installs[date_obj]["gds_tpchn_archive"] = True;
                except:
                    pass

    # Find DAQ .ini file
    for (dirpath, dirnames, filenames) in os.walk(chans_daq_archive):
        for file in filenames:
            upper_model = model.upper()+"_"
            if file.startswith(upper_model):
                try:
                    date_str = file.split(upper_model, 1)[1].strip(".ini")
                    date_obj = datetime.strptime(date_str, '%y%m%d_%H%M%S')
                    if date_obj in valid_installs:
                        valid_installs[date_obj]["daq_archive"] = True;
                except:
                    pass

    # Find backups in chans/filter_archive/
    for (dirpath, dirnames, filenames) in os.walk(chans_filter_archive):
        for file in filenames:
            upper_model = model.upper()+"_"
            if file.startswith(upper_model) and file.endswith("_install.txt"): #SDf stores backups here with no _install.txt and with gpstime
                try:
                    date_str = file.split(upper_model, 1)[1].strip("_install.txt")
                    date_obj = datetime.strptime(date_str, '%y%m%d_%H%M%S')
                    if date_obj in valid_installs:
                        valid_installs[date_obj]["filter_archive"] = True;
                except :
                    pass


    # Mark and clear out any invalid archives
    unusable_archives = []
    for key, value in valid_installs.items():
        passed = True
        for key2, value2 in value.items():
            if value2 == False:
                passed = False
        if passed == False:
            print("Partial archive at date ", key, " marking as unusable.")
            unusable_archives.append(key)
    for key in unusable_archives:
        del valid_installs[key]

    return valid_installs;

def rm_if_exists( pathname: str ):
    try:
        os.remove( pathname )
    except OSError as e:
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            print(f"Tried to remove '{pathname}', but failed.\n")
            raise e

def rm_tree_if_exists( pathname: str ):
    try:
        shutil.rmtree( pathname )
    except FileNotFoundError as e:
        pass
    except Exception as ex:
        print(f"Tried to remove (tree) '{pathname}', but failed.\n")
        raise ex


def date_obj_to_str(date_time: datetime):
    return date_time.strftime('%y%m%d_%H%M%S')


def chmod_r_add_write( path: str, perm: int):
    try:
        for root, dirs, files in os.walk(path):
            for direc in dirs:
                os.chmod(os.path.join(root, direc), perm )
            for file in files:
                os.chmod(os.path.join(root, file), perm )
    except Exception as ex:
        print(f"Failed to chmod '{path}', with perm: {perm}\n")
        raise ex

def revert_work(model: str, date_time: datetime):
    print(f"Revert for model '{model}' called using datetime: ", date_time )


    #
    # Clean up target install
    #
    target_dir = os.path.join(env.RCG_TARGET, "target", model)


    # Change permissions and rm build_info dir
    build_dir = os.path.join(target_dir, "build_info")
    if os.path.exists(build_dir ):
        chmod_r_add_write(build_dir, 0o664)
        try:
            os.chmod(build_dir, 0o774 )
        except Exception as ex:
            print(f"Failed to add execute perm to {build_dir}")
        rm_tree_if_exists(build_dir)

    rm_tree_if_exists(target_dir)

    #
    # Copy the archived target dir back
    #
    target_archive =  os.path.join(env.RCG_TARGET, "target_archive", model, model + "_" +date_obj_to_str(date_time))
    try:
        shutil.copytree(target_archive, target_dir, symlinks=True)
    except Exception as ex:
        print(f"Failed to copy '{target_archive}' to '{target_dir}'")
        raise ex


    #
    # Clean medm files and restore from archive
    #
    medm_install = os.path.join(env.RCG_TARGET, "medm", model)
    medm_archive = os.path.join(env.RCG_TARGET, "medm", "archive", model + "_" + date_obj_to_str(date_time))

    for root, dirs, files in os.walk(medm_archive):
        for file in files:
            try:
                shutil.copy2( os.path.join(medm_archive, file), os.path.join(medm_install, file), follow_symlinks=True)
            except Exception as ex:
                print(f"Failed to copy '{os.path.join(medm_archive, file)}' to '{os.path.join(medm_install, file)}'")
                raise ex


    #
    # Clean gds/param files and restore from archive
    #
    param_install = os.path.join(env.RCG_TARGET, "target", "gds", "param")
    tpchn_install = os.path.join(param_install, "tpchn_"+model+".par")

    param_archive = os.path.join(env.RCG_TARGET, "target", "gds", "param", "archive")
    testpoint_archive = os.path.join( param_archive, "testpoint_" + date_obj_to_str(date_time) + ".par" )
    tpchn_archive = os.path.join( param_archive, "tpchn_" + model + "_" + date_obj_to_str(date_time) + ".par" )

    rm_if_exists( tpchn_install )

    try :
        shutil.copy2( tpchn_archive,     tpchn_install, follow_symlinks=True )
    except Exception as ex:
        print(f"Failed to copy '{tpchn_archive}' to '{tpchn_install}'")
        raise ex
    #
    # Clean chans/daq .ini file and install from archive
    #
    daq_ini = os.path.join(env.RCG_TARGET, "chans", "daq", model.upper() + ".ini" )
    daq_ini_archive =  os.path.join(env.RCG_TARGET, "chans", "daq", "archive", model.upper() + "_" + date_obj_to_str(date_time) + ".ini")
    rm_if_exists( daq_ini )
    try :
        shutil.copy2( daq_ini_archive, daq_ini, follow_symlinks=True)
    except Exception as ex:
        print(f"Failed to copy '{daq_ini_archive}' to '{daq_ini}'")
        raise ex

    #
    # Clean and install from archive chans/filter_archive
    #
    # This file gets symlinked to userapps, but we do want to revert it and its backup is a normal file so we just copy back
    filter_file = os.path.join(env.RCG_TARGET, "chans", model.upper()+".txt")
    filter_file_archive = os.path.join(env.RCG_TARGET, "chans", "filter_archive", model,  model.upper() + "_" + date_obj_to_str(date_time) + "_install.txt")
    try :
        shutil.copy2( filter_file_archive, filter_file, follow_symlinks=True)
    except Exception as ex:
        print(f"Failed to copy '{filter_file_archive}' to '{filter_file}'")
        raise ex



def revert_model(model: str, last_good: bool):
    """
    Revert a model to an older installation
    :param model: The name of the model
    :param last_good: When true, automatically pick the latest good backup to revert from
    :return: True if revert was successful
    """

    archives = find_installations(model)
    count = 0
    keys = []

    if len(archives) < 1:
        click.echo(f"No suitable archives found for : {model}")
        return False

    if last_good == False:
        click.echo("Please select the archive you would like to revert to:")

    for date, item in sorted(list(archives.items())): #We sort so newest is the default
        if last_good == False:
            click.echo(f"{count} : {date}")
        keys.append(date)
        count += 1

    if last_good == False:
        archive_index = click.prompt(f'Please enter a valid archive index: ', type=int, default=count-1)
    else:
        archive_index = count-1

    if archive_index < 0 or archive_index > count-1:
        click.echo(f"Error : archive_index {archive_index} is not valid, aborting revert attempt.")
        return False

    revert_work(model, keys[archive_index])

    return True


def revert_models(models: list, last_good: bool):
    """
    Revert a list of models to an older installation
    :param models: A list of models
    :param last_good: When true, automatically pick the latest good backup to revert from
    :return: True if all reversions were successful
    """
    all_good = True
    for model in models:
        reverted = revert_model(model, last_good)
        all_good = all_good and reverted
    return all_good


@click.command()
@click.argument("model", nargs=-1
                )
@click.option("-last-good", "--last-good", default=False, is_flag=True,
              help='Automatically selects the last good build to revert to')
@click.option("-a", "--all", "all_", default=False, is_flag=True,
              help='build all models'
              )
def revert(model, last_good, all_):
    """
    revert one or more models to an older install

    MODEL: the name of one or more models
    """
    env.check_environment()
    if os.getuid() == 0:
        error("reverting as 'root' not allowed")

    if all_:
        model = list_models()

    if len(model) < 1:
        error("Must pass at least one model to the revert command")

    success = revert_models(model, last_good)
    if not success:
        sys.exit(1)



