# install models for rtcds
import sys
import os
import subprocess

import click

from .env import environment as env
from .util import list_models
from .log import log, error


install_results = {}


def install_model(model: str):
    """
    Install a model
    :param model: The name of the model
    :return: True if installation successful
    """
    os.chdir(env.RCG_BUILDD)
    log(f"### installing {model}")
    cmd = ["make", f"install-{model}"]
    environ = env.get_environment()
    result = subprocess.run(cmd, env=environ)

    global install_results
    install_results[model] = result
    return result.returncode == 0


def install_summary():
    """
    Summarize installation results to screen
    :return: number of failed installs
    """
    global install_results

    success = []
    failed = []

    total = len(install_results)

    for model, result in install_results.items():
        if result is not None and result.returncode == 0:
            success.append(model)
        else:
            failed.append(model)

    if len(success) > 0:
        print(f"{len(success)} of {total} models installed successfully: {' '.join(success)}")
    if len(failed) > 0:
        print(f"{len(failed)} of {total} models failed to install: {' '.join(failed)}")
    return len(failed)


def install_all():
    """
    Install every model for the host, or every model on the whole IFO if run on boot server or build server
    :return:
    """
    os.chdir(env.RCG_BUILDD)
    log("### installing all models")
    cmd = ["make", "installWorld"]
    subprocess.run(cmd, env=env.get_environment())


def install_body(model):
    """Install list of models

    :return : True if all models installed successfully
    """
    env.check_environment()
    all_success = True
    global install_results

    try:
        for m in model:
            install_results[m] = None
            if m.lower() != m:
                click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
            else:
                success = install_model(m)
                all_success = all_success and success
    except KeyboardInterrupt:
        print("\nInterrupted from keyboard")

    return all_success


@click.command()
@click.argument("model", nargs=-1
                )
@click.option("-a", "--all", "all_", default=False, is_flag=True,
              help='build all models'
              )
def install(model, all_):
    """
    install one or more models

    MODEL: the name of one or more models
    """
    env.check_environment()
    if os.getuid() == 0:
        error("installing as 'root' not allowed")
    if all_:
        model = list_models()
    install_body(model)
    num_failed = install_summary()
    if num_failed > 0:
        sys.exit(1)

