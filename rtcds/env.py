# gather environment variables into a module
# exports the env object.
# variables are attributes of the object
# USE_KERNEL_MODEL={true:false} becomes env.USE_KERNEL_MODEL: bool
from os import environ as start_environ
import os.path as path
import sys

import click
import platform
from dotenv import dotenv_values


class _EnvClass(object):
    """
    Extract relevant environment variables, converting to Python types and setting
    defaults as needed.
    """
    def __init__(self):
        self.variables = []
        self.required = []
        self.export = []
        self.order = []

    def add_bool(self, name: str, default: bool, help_=None):
        """
        Add a variable set to "true" or "false" as a bool
        :param name: name of Environment variable
        :param default:
        :param help_: A help string
        :return: None
        """
        if name in environ and len(environ[name]) > 0:
            value = environ[name] == "true"
        else:
            value = default
        self._set_variable_attributes(name, value, help_)

    def add_str(self, name, default="", required=False, help_=None,
                upper=False, lower=False, ignore_environment=False,
                export=False):
        """
        Add a variable copying the value from the environment directly as a string
        :param name:
        :param default:
        :param required: if true, an error is reported when the variable is undefined and the environment is checked.
        :param help_: A help string
        :param upper: make sure value is uppercase
        :param lower: make sure value is lowercase
        :param ignore_environment: take default value.  Ignore value in environment
        :param export: Export this to the environment of some commands
        :return:
        """
        up_name = name.upper()
        if not ignore_environment and up_name in environ and len(environ[up_name]) > 0:
            value = environ[up_name]
        else:
            value = default
        if required:
            self.required.append(name)
        if export:
            self.export.append(name)
        if upper and lower:
            raise Exception("'upper' and 'lower' cannot both be true when adding an environment variable")
        if value is not None and upper:
            value = value.upper()
        if value is not None and lower:
            value = value.lower()
        self._set_variable_attributes(name, value, help_)

    def add_int(self, name, default=0, required=False, help_=None, export=False):
        """
        Add a variable that's supposed to be an integer.  If it's not, then throw an error.
        :param name:
        :param default:
        :param required: if true, an error is reported when the variable is undefined and the environment is checked.
        :param help_: A help string
        :param export: Export this to the environment of some commands
        :return:
        """
        if name in environ and len(environ[name]) > 0:
            try:
                value = int(environ[name])
            except ValueError:
                click.echo(f"environment variable {name} must be an integer")
                sys.exit(1)
        else:
            value = default
        if required:
            self.required.append(name)
        if export:
            self.export.append(name)
        self._set_variable_attributes(name, value, help_)

    def _set_variable_attributes(self, name, value, help_):
        self.order.append(name)
        self.variables.append(name)
        setattr(self, name, value)
        self._add_check_func(name)
        self._set_help(name, help_)

    def _set_help(self, name: str, help_):
        if help_ is not None:
            setattr(self, f"{name}_help", help_)
        else:
            setattr(self, f"{name}_help", None)

    def _get_help(self, name: str):
        """
        Get the help string for a variable or None
        :param name: name of environment variable
        :return: help string if it exists or None
        """
        return getattr(self, f"{name}_help")

    def _add_check_func(self, name: str):
        """
        Add a check function for a specific environment variable.

        :param name:
        :return:
        """
        setattr(self, f"check_{name}",
                lambda slf: slf._check_var(name))

    def _check_var(self, name):
        """Check if a particular name is in the environment, if not quit with error"""
        if name not in environ:
            msg = f"{name} could not be found in environment but is required"
            help_ = self._get_help(name)
            if help_ is not None:
                msg += f"\n{name} is {help_}"
            raise click.ClickException(msg)

    def print(self):
        """
        Print all environment variables
        :return:
        """
        for name in self.order:
            value = getattr(self, name)
            if value is None:
                click.echo(f"{name}=")
            else:
                click.echo(f"{name}={value}")

    def check_environment(self):
        """
        Check if required variables are set.  If not, print an error message and quit.
        :return:
        """
        msg = []
        for name in self.required:
            value = self.__getattribute__(name)
            if value is None or len(value) <= 0:
                msg .append(f"{name} environment variable not set, but is required")
                h = self._get_help(name)
                if h is not None and len(h) > 0:
                    msg.append(f"\t{name} is {h}")
        if len(msg) > 0:
            raise click.ClickException("\n".join(msg))

    def _get_exports(self):
        """
        Return all exported environment variables as a dictionary
        :return: A dict of the exported variables.
        """
        return {n: self.__getattribute__(n) for n in self.export}

    def get_environment(self):
        """
        Get the original environment merged with exported variables from this object
        :return:
        """
        exp = self._get_exports()
        new_env = {v: environ[v] for v in environ}
        for n, v in exp.items():
            new_env[n] = v
        return new_env


def FindRCGVersion():
    """
    Get the RCG version as a string in case it's not specified in the environment.

    :return:
    """
    # First, check if there's an installed version number.  If so, use it.
    try:
        with open(f"{environment.RCG_SRC}/rcg-version", "rt") as f:
            lines = f.readlines()
            ver = lines[0].strip()
        if len(ver) > 0:
            return ver
    except IOError:
        pass

    # then try to parse src version number
    try:
        with open(f"{environment.RCG_SRC}/src/include/rcgversion.h", "rt") as f:
            lines = f.readlines()
            major = None
            minor = None
            sub = None
            release = None
            for line in lines:
                words = line.split()
                if len(words) >= 3:
                    if words[1] == "RCG_VERSION_MAJOR":
                        major = int(words[2])
                    if words[1] == "RCG_VERSION_MINOR":
                        minor = int(words[2])
                    if words[1] == "RCG_VERSION_SUB":
                        sub = int(words[2])
                    if words[1] == "RCG_VERSION_REL":
                        rel_num = int(words[2])
                        if rel_num == 0:
                            release = "devel"
                        else:
                            release = "release"
            if major is not None and minor is not None and sub is not None and release is not None:
                return f"{major}.{minor}.{sub}~{release}"
    except IOError:
        pass
    except ValueError:
        pass

    # finally, return some default
    return "noversion"


def LoadExtraEnvironmentFiles(files: list):
    """
    Load variables from environment files.

    Variable definitions in later files will override definitions of the same variable in earlier files.
    :param files: A list of file paths
    :return: A dictionary collecting definitions from each file
    """
    all_file_vars = {}
    for file in files:
        if path.isfile(file):
            try:
                file_vars = dotenv_values(file)
                all_file_vars = {**all_file_vars, **file_vars}
            except Exception as e:
                click.echo(f"WARNING: Could not read environment file {file}: {e}")
        else:
            pass
    return all_file_vars


# load in extra environment files
# important to put host-specific file second so it's variables override the generic file
env_files = ["/etc/advligorts/systemd_env", f"/etc/advligorts/systemd_env_{platform.node()}"]
vars_from_files = LoadExtraEnvironmentFiles(env_files)

# let variables from the environment override variables from the env files
environ = {**vars_from_files, **start_environ}

# create object
environment = _EnvClass()

# add variables

# site and ifo info
environment.add_str("SITE", upper=True, required=True, export=True,
                    help_="a three-letter site abbreviation")
environment.add_str("site", lower=True, export=True)
environment.add_str("IFO", upper=True, required=True, export=True,
                    help_="a two-letter interferometer abbreviation")
environment.add_str("ifo", lower=True, export=True)

# RCG source directory
environment.add_str("RCG_SRC", default="/usr/share/advligorts/src", export=True)

# Version of RCG
found_version = FindRCGVersion()
environment.add_str("RTS_VERSION", default=found_version)
if found_version != environment.RTS_VERSION:
    print(f"WARNING: RTS_VERSION={environment.RTS_VERSION} does not match version found in source: {found_version}")

environment.add_str("RCG_LIB_PATH", export=True, required=True,
                    help_="a list of paths to search for model source files")
if len(environment.RCG_LIB_PATH):
    environment.RCG_LIB_PATH += f":{environment.RCG_SRC}/src/epics/simLink/:{environment.RCG_SRC}/src/epics/simLink/lib"

# error out if model is not supposed to run on host
environment.add_bool("CHECK_MODEL_IN_HOST", default=False)

# should be true if the computer is dolphin
environment.add_bool("IS_DOLPHIN_NODE", default=False)

# dolphin product generation code
environment.add_str("DOLPHIN_GEN", default="ix")

# true if the host should stream data to the DAQ (normal front end or edc)
environment.add_bool("DAQ_STREAMING", default=False)

# whether to build kernel models or user models by default.
environment.add_bool("USE_KERNEL_MODELS", default=True)


# Root of default build directories
environment.add_str("RCG_BUILD_ROOT", default="/var/cache/advligorts",
                    help_="root of defult build directories")

# RCG build directory
environment.add_str("RCG_BUILDD", default=f"{environment.RCG_BUILD_ROOT}/rcg-{environment.RTS_VERSION}", export=True)

# model installation directory
environment.add_str("RCG_TARGET", default=f"/opt/rtcds/{environment.site}/{environment.ifo}", export=True)

# locations for model files
environment.add_str("CDS_SRC", export=True,
                    default="RCG_LIB_PATH" in environ and environ["RCG_LIB_PATH"] or "")
environment.add_str("CDS_IFO_SRC", export=True, default=environment.CDS_SRC)


# allow RTCDS to enable models at start in systemd
environment.add_bool("ALLOW_MODEL_ENABLE", default=True)

# install paths
environment.add_str("CDS_MEDM_PATH", export=True, default=f"{environment.RCG_TARGET}/medm/templates")
environment.add_str("CDS_SCRIPTS_PATH", export=True, default=f"{environment.RCG_TARGET}/post_build")

# number of seconds to wait after starting a model
environment.add_int("START_DELAY", default=15)

# models that only have an epics process
# these models don't have a kernel module or awgtpman
environment.add_str("EPICS_ONLY_MODELS", export=True, default="")

# Model names can optionally be defined in a environment variable
environment.add_str("IOP_MODEL", default="")
environment.add_str("USER_MODELS", default="")


@click.command()
def env():
    """
    Check for required environment variables, then
    show relevant environment variables
    """
    environment.check_environment()
    environment.print()
