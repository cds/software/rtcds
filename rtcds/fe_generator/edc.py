from .systemd import SystemdService

class EDC(object):
    def __init__(self, options):
        if options["HAS_EDC"]:
            self.edcs = [SystemdService(f"rts-edc_{edc}") for edc in options["EDC"]]
            for i in range(1, len(self.edcs)):
                self.edcs[i].after(self.edcs[i-1])

        else:
            self.edcs = []

