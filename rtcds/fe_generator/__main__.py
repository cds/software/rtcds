# Generate systemd unit files needed for LIGO CDS front-end
# server startup sequences

import sys

from .._version import version
from .sequencer import Sequencer
from .options import get_options
from .log import klog


def main():
    klog(f"version {version} in python with args {sys.argv}")
    seq = Sequencer(get_options(), sys.argv[2])
    klog(f"Sequencer created")
    seq.create_start_sequence()
    klog(f"sequence complete")


if __name__ == '__main__':
    main()
