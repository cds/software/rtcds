#!/bin/bash

# need to specify NETWORK_DIR in all such tests
# since defualt goes to /run/systemd/network
mkdir -p test/network
export NETWORK_DIR=test/network

export IS_DOLPHIN_NODE=false

export PYTHONPATH=../..
python3 -m rtcds.fe_generator dummy test dummy2