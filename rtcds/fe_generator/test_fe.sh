#!/bin/bash

# need to specify NETWORK_DIR in all such tests
# since defualt goes to /run/systemd/network
mkdir -p test/network
export NETWORK_DIR=test/network

export IS_DOLPHIN_NODE=true
export START_MODELS=true
export USE_DOLPHIN_DAEMON=true
export IOP_MODEL=testiop
export USER_MODELS="model1 model2"
export EDC="testedc"
export DAQ_STREAMING=true
export cps_xmit_args=test_args
export DAQ_MTU=8888

export DAQ_ETH_DEV=eth1
export DAQ_ETH_IP=10.0.0.1

export PYTHONPATH=../..
python3 -m rtcds.fe_generator dummy test dummy2