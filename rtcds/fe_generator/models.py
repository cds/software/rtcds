from .systemd import SystemdService, SystemdTarget
from .log import klog
from .delay import Delay


class Model(object):
    """
    Represents the services needed to start a single model
    """
    def __init__(self, name):

        # create service object for the three parts of a model
        self.epics = SystemdService(f"rts-epics@{name}")
        self.delay = Delay(name, 2, 0)
        self.module = SystemdService(f"rts-module@{name}")
        self.awg = self.first = SystemdService(f"rts-awgtpman@{name}")

        # module must be running for awg to run
        self.awg.binds_to(self.module)

        # module should start after epics, but can run on its own
        self.module.require_after(self.delay)
        self.delay.binds_to(self.epics)

        # cut off epics process quick.  Some hang with caRepeater process
        self.epics.stop_timeout = 5

        self.target = SystemdTarget(f"rts@{name}")

        self.target.add_unit(self.epics, self.module, self.awg)


class EpicsOnlyModels(object):
    """
    Represents an epics only model: now kernel module, no awg
    """
    def __init__(self, models):
        self.models = [SystemdService(f"rts-epics@{name}") for name in models]
        if len(self.models) > 0:
            for i in range(1, len(self.models)):
                self.models[i].after(self.models[i-1])
        self.target = SystemdTarget("rts-epics-only-models.target","""[Unit]
Description=All epics only models
""")
        self.target.add_unit(*self.models)


class IOPModel(object):
    def __init__(self, name):
        self.model = Model(name)
        self.target = SystemdTarget("rts-iop-model","""[Unit]
Description=The IPO model.
""")
        self.target.add_unit(self.model.target)


class UserModels(object):
    def __init__(self, iop, names):
        models = []
        for name in names:
            models.append(Model(name))

        self.target = SystemdTarget("rts-user-models", """[Unit]
Description=All user models
""")
        self.target.add_unit(*[m.target for m in models])

        for model in models:
            model.epics.binds_to(iop.model.module)
            model.module.binds_to(iop.model.module)

        for i in range(1,len(models)):
            models[i].epics.after(models[i-1].module)

        if iop.target is not None:
            self.target.binds_to(iop.target)

class Models(object):
    def __init__(self, options):
        sub_targets = []
        if options["HAS_EPICS_ONLY_MODELS"]:
            klog("creating units for epics-only models: " + " ".join(options["EPICS_ONLY_MODELS"]))
            self.epics_only_models = EpicsOnlyModels(options["EPICS_ONLY_MODELS"])
            sub_targets.append(self.epics_only_models.target)
        else:
            self.epics_only_models = None
        if options["HAS_IOP_MODEL"]:
            klog("creating units for iop model " + options["IOP_MODEL"])
            self.iop = IOPModel(options["IOP_MODEL"])
            sub_targets.append(self.iop.target)
        else:
            self.iop = None
        if options["HAS_USER_MODELS"]:
            if self.iop is not None:
                klog("creating units for user models: " + " ".join(options["USER_MODELS"]))
                user_models = UserModels(self.iop, options["USER_MODELS"])
                sub_targets.append(user_models.target)
            else:
                klog("Can't have user models without an IOP model")
        else:
            user_models = None

        if len(sub_targets) > 0:
            self.target = SystemdTarget("rts-models","""[Unit]
Description=All models
""")
            self.target.add_unit(*sub_targets)
        else:
            self.target = None
