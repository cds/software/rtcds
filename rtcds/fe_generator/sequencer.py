# Set the start sequence for front ends and other diskless systems

import os.path as path
from .log import klog

from .dolphin import Dolphin
from .world import World

from .systemd import output_units

from .delay import Delay

class Sequencer(object):
    def __init__(self, options, target_dir):
        """options is a dictionary of host-specific key/value pairs that will determine what
        elements are added to the start sequence

        target_dir is the target directory for unit files
        """
        self.options = options
        self.target_dir = target_dir
        self.network_dir = options["NETWORK_DIR"]

    def create_start_sequence(self):


        dolphin = Dolphin(self.options)
        world = World(self.options)

        # 30 second from startup to model load minimum
        start_delay = Delay("startup", 30, 10)

        start_delay.wanted_by("multi-user.target")

        for dolphin_tail in dolphin.after_binds:
            start_delay.binds_to(dolphin_tail)

        if world.first_service is not None:
            world.first_service.binds_to(start_delay)

        if world.target is not None:
            world.target.after(start_delay)

        # make sure targets start
        if dolphin.target is not None:
            dolphin.target.wanted_by("multi-user.target")

        if self.options["START_MODELS"]:
            world.target.wanted_by("multi-user.target")
        else:
            klog("START_MODELS is false.  Skipping model start.")

        klog(f"Writing unit files to {self.target_dir}")
        klog(f"Writing network files ot {self.network_dir}")

        try:
            output_units(self.target_dir, self.network_dir)
        except Exception as e:
            klog(str(e))


    def create_frontend_start_sequence(self):
        before_world = []
        world = []
        models = False
        if self.options["IS_DOLPHIN_NODE"]:
            dolphin_drivers = self.processes.dolphin_drivers(self.options["DOLPHIN_GEN"])
            if self.options["HAS_DOLPHIN_PORT"] and self.options["USE_DOLPHIN_PORT_CONTROL"]:
                dolphin_port = self.processes.dolphin_port()

                # bind drivers to port control so that drivers automatically get disabled
                # when port is closed
                self.processes.binds_to(dolphin_drivers.first_service, dolphin_port.start)

                before_world.append(dolphin_port)
                before_world.append(self.delay(15, 5, "dolphin_port"))
            before_world.append(dolphin_drivers)
            klog(f"{self.options['DOLPHIN_GEN']} gen. dolphin drivers added")
        before_world.append(self.delay(30, 10, "startup"))
        world += self.add_dolphin_daemon()
        if self.options["HAS_EPICS_ONLY_MODELS"]:
            world.append(self.processes.epics_only_models(self.options["EPICS_ONLY_MODELS"]))
            models = True
        if self.options["HAS_IOP_MODEL"]:
            world.append(self.processes.iop_model(self.options["IOP_MODEL"],
                                                  self.options["IS_DOLPHIN_NODE"]))
            models = True
        if self.options["HAS_EDC"]:
            world.append(self.processes.edcs(self.options["EDC"]))
        if self.options["DAQ_STREAMING"]:
            world.append(self.processes.streaming(self.options))
        if self.options["HAS_USER_MODELS"]:
            if self.options["HAS_IOP_MODEL"]:
                world.append(self.processes.user_models(self.options["USER_MODELS"],
                                                        self.options["IOP_MODEL"]))
                models = True
            else:
                klog("Can't have user models without an IOP model")
        if models:
            self.processes.models()
        return before_world, world

    def create_cdsrfm_start_sequence(self, front_end_processes, cdsrfm_processes):
        if self.options["USE_DOLPHIN_PORT_CONTROL"]:
            before_world = [
                front_end_processes.dolphin_port(),
                self.delay(15, 5, "dolphin_port"),
            ]
        else:
            before_world = []
        before_world += [
            front_end_processes.dolphin_drivers(self.options["DOLPHIN_GEN"]),
            self.delay(30, 10, "startup"),
        ]
        world = self.add_dolphin_daemon()
        world += [
            cdsrfm_processes.module(),
            cdsrfm_processes.epics(),
        ]
        return before_world, world

    def link_sequence(self, before_world, world, start_models):
        self.processes.create_world_target()

        # link the first of each process to multi-user or to the world target
        for process in before_world:
            self.processes.link_to(process.start, "multi-user.target")
        for process in world:
            self.processes.link_to(process.start, "rts-world.target")
            self.processes.part_of(process.start, "rts-world.target")
        if len(before_world) > 0:
            self.processes.after("rts-world.target", before_world[-1].end)
        if start_models:
            self.processes.link_to("rts-world.target", "multi-user.target")
        else:
            klog("START_MODELS is false.  Skipping model start.")
        self.processes.serialize_processes(before_world + world)

    def add_dolphin_daemon(self):
        """
        If requested, return an array of tasks that will load the dolphin daemon and module.
        :return: And list of tasks to be linked in to the sequence.  Will be an empty list
        if dolphin daemon not requested.
        """
        if self.options["IS_DOLPHIN_NODE"] and self.options["USE_DOLPHIN_DAEMON"]:

            create_dolphin_km_unit_file(self.target_dir)
            return [
                Process("rts-dolphin-proxy.service"),
                Process("rts-dolphin_daemon.service"),
                self.delay(5, 5, "dolphin_daemon")
            ]
        else:
            return []

    class Delay(object):
        def __init__(self, unit_name):
            self.start = unit_name
            self.end = unit_name
            self.first_service = unit_name

    def delay(self, start_time_s, stop_time_s, name):
        file_name = f"rts-delay-{name}.service"
        file_path = path.join(self.target_dir, file_name)
        with open(file_path, "wt") as f:
            f.write(f"""[Unit]
Description=Delay for {start_time_s} seconds

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStartPre=/bin/sleep {start_time_s}
ExecStart=/bin/echo '[startup] finished waiting for {name}'
ExecStop=/bin/sleep {stop_time_s}
ExecStopPost=/bin/echo '[shutdown] finished waiting for {name}'
""")
        return self.Delay(file_name)
