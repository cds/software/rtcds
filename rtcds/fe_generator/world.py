from .systemd import SystemdTarget
from .models import Models
from .edc import EDC
from .cdsrfm import CDSRFM
from .log import klog
from .daq_streaming import DAQStreaming

class World(object):
    """
    create world target
    """
    def __init__(self, options):
        self.first_service = None
        if options["CDSRFM"]:
            subtargets = self.handle_cdsrfm(options)
        else:
            subtargets = self.handle_frontend(options)

        # only create a world target if it's going to start something
        if len(subtargets) > 0:
            self.target = SystemdTarget("rts-world", """[Unit]
Description=All model and streaming services for Front End servers
""")
            self.target.add_unit(*subtargets)
        else:
            klog("Not creating rts-world.target.  Nothing to run.")
            self.target = None

    def handle_cdsrfm(self, options):
        """Setup world target for CDSRFM"""
        klog("is CDSRFM host")
        cdsrfm = CDSRFM()

        # set service to link dolphin startup to
        self.first_service = cdsrfm.module

        return [cdsrfm.module, cdsrfm.epics]

    def handle_frontend(self, options):
        """
        Setup world target for front ends
        :return:
        """
        klog("is standard front end host")
        models = Models(options)
        edc = EDC(options)
        streaming = DAQStreaming(options)

        sub_targets = []
        if models.target is not None:
            sub_targets.append(models.target)
        if len(edc.edcs) > 0:
            sub_targets += edc.edcs
        if streaming.target is not None:
            sub_targets.append(streaming.target)

        # set service to link dolphin startup in this priority:
        # 1. iop  2. first epics only model 3. edc
        if models is not None:
            if models.iop is not None:
                self.first_service = models.iop.model.epics
            if models.epics_only_models is not None and len(models.epics_only_models.models) > 0:
                if self.first_service is None:
                    self.first_service = models.epics_only_models.models[0]
                else:
                    models.epics_only_models.models[0].after(self.first_service)
        if len(edc.edcs) > 0:
            if self.first_service is None:
                self.first_service = edc.edcs[0]
            else:
                edc.edcs[0].after(self.first_service)

        return sub_targets